#ifndef MAINCLASS_H
#define MAINCLASS_H

#include <QCoreApplication>
#include <QDebug>
#include <QObject>
#include <QSocketNotifier>
#include <QtSerialPort/QtSerialPort>

class MainClass : public QObject {
  Q_OBJECT
private:
  QCoreApplication *app;
  QSocketNotifier notifier;
  QSerialPort *serial;
  bool need_quit = false;

public:
  explicit MainClass(QObject *parent = 0);
  ~MainClass();
  void quit();
signals:
  void finished();
  void textReceived(QString message);

public slots:
  void run();
  void aboutToQuitApp();
  void text();
};

#endif // MAINCLASS_H
