#include <QCoreApplication>
#include <QTimer>

#include "mainclass.h"

int main(int argc, char *argv[]) {
  QCoreApplication app(argc, argv);

  MainClass myMain;

  QObject::connect(&myMain, SIGNAL(finished()), &app, SLOT(quit()));
  QObject::connect(&app, SIGNAL(aboutToQuit()), &myMain,
                   SLOT(aboutToQuitApp()));

  return app.exec();
}
