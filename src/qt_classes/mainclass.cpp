#include "mainclass.h"
#include <QTimer>
#include <unistd.h>

#include "foo.h"

MainClass::MainClass(QObject *parent)
    : QObject(parent), notifier(STDIN_FILENO, QSocketNotifier::Read) {
  app = QCoreApplication::instance();
  QTimer::singleShot(100, this, SLOT(run()));
  connect(&notifier, SIGNAL(activated(int)), this, SLOT(text()));
  serial = new QSerialPort();
  foo *f = new foo();
}

MainClass::~MainClass() { delete serial; }

void MainClass::quit() { emit finished(); }

void MainClass::run() {

  if (need_quit) {
    quit();
    return;
  }

  QTimer::singleShot(10, this, SLOT(run()));
}

void MainClass::aboutToQuitApp() {}

void MainClass::text() {
  QTextStream qin(stdin);
  QString line = qin.readLine();

  qDebug() << line;

  if (line == "q")
    need_quit = true;

  emit textReceived(line);
}
